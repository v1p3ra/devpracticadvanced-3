#### Практическое задание №2
#### 1. Количество записей в инвентаре в таком-то складе для такого-то фильма:
```sql
select count(*)
from inventory
where film_id = 73 and store_id = 1;
```

План запроса без индексов:
```sql
Aggregate  (cost=93.73..93.74 rows=1 width=8) (actual time=0.278..0.278 rows=1 loops=1)
  ->  Seq Scan on inventory  (cost=0.00..93.72 rows=4 width=0) (actual time=0.029..0.275 rows=4 loops=1)
        Filter: ((film_id = 73) AND (store_id = 1))
        Rows Removed by Filter: 4577
Planning Time: 0.055 ms
Execution Time: 0.295 ms
```

Индексы:
```sql
create index IDX_FILM_ID_ON_INVENTORY on inventory (film_id);
create index IDX_STORE_ID_ON_INVENTORY on inventory (store_id);
```

План запроса с индексом IDX_FILM_ID_ON_INVENTORY (ускорение в 0,295/0,036 мс = 8,20 раз):
```sql
Aggregate  (cost=8.45..8.46 rows=1 width=8) (actual time=0.017..0.017 rows=1 loops=1)
  ->  Index Scan using idx_film_id_on_inventory on inventory  (cost=0.28..8.44 rows=4 width=0) (actual time=0.014..0.015 rows=4 loops=1)
        Index Cond: (film_id = 73)
        Filter: (store_id = 1)
        Rows Removed by Filter: 4
Planning Time: 0.072 ms
Execution Time: 0.036 ms
```

План запроса с дополнительным индексом IDX_STORE_ID_ON_INVENTORY (без изменений):
```sql
Aggregate  (cost=8.45..8.46 rows=1 width=8) (actual time=0.015..0.016 rows=1 loops=1)
  ->  Index Scan using idx_film_id_on_inventory on inventory  (cost=0.28..8.44 rows=4 width=0) (actual time=0.012..0.013 rows=4 loops=1)
        Index Cond: (film_id = 73)
        Filter: (store_id = 1)
        Rows Removed by Filter: 4
Planning Time: 0.079 ms
Execution Time: 0.032 ms
```

#### Объяснение:
Причины использования Index Scan: упорядоченность значений в столбце film_id (значения расположены по возрастанию) и высокая селективность условия.
В таблице 4581 запись. Благодаря фильтрам "film_id = 73" и "store_id = 1" осталось 4 записи.

```sql
select reltuples from pg_class where relname='inventory';
```

```
reltuples|
---------|
     4581|
```

```sql
select correlation
from pg_stats
where tablename = 'inventory' and attname = 'film_id';
```

```
correlation|
-----------|
          1|
```

Добавление индекса IDX_STORE_ID_ON_INVENTORY не изменило план выполнения, так как в столбце store_id во всей таблице встречаются всего 2 различных значения, частота появления каждого из которых составляет ~50% (средняя селективность выборки):
```sql
select attname, most_common_vals, most_common_freqs
from pg_stats
where tablename = 'inventory' and attname = 'store_id';
```

```
attname |most_common_vals|most_common_freqs  |
--------|----------------|-------------------|
store_id|{2,1}           |{0.504475,0.495525}|
```



#### 2. Выбор id записей инвентаря для такого-то клиента, взятых после такой-то даты:
```sql
select inventory_id
from rental
where customer_id = 236 and rental_date > '2005-08-20';
```

План запроса без индексов:
```sql
Seq Scan on rental  (cost=0.00..390.66 rows=7 width=4) (actual time=0.955..1.193 rows=4 loops=1)
  Filter: ((rental_date > '2005-08-20 00:00:00'::timestamp without time zone) AND (customer_id = 236))
  Rows Removed by Filter: 16040
Planning Time: 0.053 ms
Execution Time: 1.203 ms
```

Индексы:
```sql
create index IDX_CUSTOMER_ID_ON_RENTAL on rental (customer_id);
create index IDX_RENTAL_DATE_ON_RENTAL on rental (rental_date);
```

План запроса с индексом IDX_CUSTOMER_ID_ON_RENTAL (ускорение в 1,203/0,053 мс = 22,7 раз):
```sql
Bitmap Heap Scan on rental  (cost=4.60..98.10 rows=7 width=4) (actual time=0.035..0.037 rows=4 loops=1)
  Recheck Cond: (customer_id = 236)
  Filter: (rental_date > '2005-08-20 00:00:00'::timestamp without time zone)
  Rows Removed by Filter: 38
  Heap Blocks: exact=35
  ->  Bitmap Index Scan on idx_customer_id_on_rental  (cost=0.00..4.60 rows=42 width=0) (actual time=0.010..0.010 rows=42 loops=1)
        Index Cond: (customer_id = 236)
Planning Time: 0.067 ms
Execution Time: 0.053 ms
```

План запроса с дополнительным индексом IDX_RENTAL_DATE_ON_RENTAL (ускорение в 1,203/0,103 мс = 11,7 раз):
```sql
Bitmap Heap Scan on rental  (cost=57.29..80.86 rows=7 width=4) (actual time=0.084..0.086 rows=4 loops=1)
  Recheck Cond: ((customer_id = 236) AND (rental_date > '2005-08-20 00:00:00'::timestamp without time zone))
  Heap Blocks: exact=3
  ->  BitmapAnd  (cost=57.29..57.29 rows=7 width=0) (actual time=0.081..0.081 rows=0 loops=1)
        ->  Bitmap Index Scan on idx_customer_id_on_rental  (cost=0.00..4.60 rows=42 width=0) (actual time=0.010..0.010 rows=42 loops=1)
              Index Cond: (customer_id = 236)
        ->  Bitmap Index Scan on idx_rental_date_on_rental  (cost=0.00..52.44 rows=2687 width=0) (actual time=0.067..0.067 rows=2689 loops=1)
              Index Cond: (rental_date > '2005-08-20 00:00:00'::timestamp without time zone)
Planning Time: 0.074 ms
Execution Time: 0.103 ms
```

План запроса с индексом IDX_RENTAL_DATE_ON_RENTAL (ускорение в 1.,203/0,342 мс = 3,5 раза):
```sql
Index Scan using idx_rental_date_on_rental on rental  (cost=0.29..117.56 rows=7 width=4) (actual time=0.111..0.333 rows=4 loops=1)
  Index Cond: (rental_date > '2005-08-20 00:00:00'::timestamp without time zone)
  Filter: (customer_id = 236)
  Rows Removed by Filter: 2685
Planning Time: 0.067 ms
Execution Time: 0.342 ms
```

#### Объяснение:

Были рассмотрены три возможных сочетания индексов. Наиболее эффективным при таком запросе является использование индекса IDX_CUSTOMER_ID_ON_RENTAL: значения в столбце customer_id не упорядочены (значения распределены случайно), селективность условия высокая (Bitmap Index Scan было отобрано 42 записи из 16044, затем отфильтровано (удалено) ещё 38 записей).

```sql
select reltuples from pg_class where relname='rental';
```

```
reltuples|
---------|
    16044|
```

```sql
select correlation
from pg_stats
where tablename = 'rental' and attname = 'customer_id';
```

```
correlation|
-----------|
 0.00253747|
```

Добавление индекса IDX_RENTAL_DATE_ON_RENTAL снизило эффективность, так как привело к составлению ещё одной битовой карты и последующему объединению битовых карт.
Использование одного индекса IDX_RENTAL_DATE_ON_RENTAL дало худший результат, чем в предыдущих двух случаях, так как применялся Index Scan, а при заданных условиях запроса было отобрано 2689 записей, 2685 которых впоследствии были отфильтрованы (удалены). Index Scan использовался из-за упорядоченности значений в столбце rental_date и высокой селективности выборки (83,2%):
```sql
select correlation
from pg_stats
where tablename = 'rental' and attname = 'rental_date';
```

```
correlation|
-----------|
 0.99778163|
```



#### 3. Перечисление всех фильмов с такой-то ценой аренды:
```sql
select title, description
from film
where rental_rate = 2.99;
```

План запроса без индекса:
```sql
Seq Scan on film  (cost=0.00..66.50 rows=323 width=109) (actual time=0.011..0.207 rows=323 loops=1)
  Filter: (rental_rate = 2.99)
  Rows Removed by Filter: 677
Planning Time: 0.050 ms
Execution Time: 0.220 ms
```

Индекс:
```sql
create index IDX_RENTAL_RATE_ON_FILM on film (rental_rate);
```

План запроса с индексом IDX_RENTAL_RATE_ON_FILM (без изменений):
```sql
Seq Scan on film  (cost=0.00..66.50 rows=323 width=109) (actual time=0.011..0.204 rows=323 loops=1)
  Filter: (rental_rate = 2.99)
  Rows Removed by Filter: 677
Planning Time: 0.058 ms
Execution Time: 0.218 ms
```

Объяснение:
Добавление индекса IDX_RENTAL_RATE_ON_FILM не привело к изменению плана запроса, поскольку селективность условия в запросе составила менее 80%: отобрано 323 записи из 1000 (селективность условия - 67,7%), данные распределены случайно.
```sql
select reltuples from pg_class where relname='film';
```

```
reltuples|
---------|
     1000|
```

```sql
select attname, most_common_vals, most_common_freqs
from pg_stats
where tablename = 'film' and attname = 'rental_rate';
```

```
attname    |most_common_vals|most_common_freqs            |
-----------|----------------|-----------------------------|
rental_rate|{0.99,4.99,2.99}|{0.34099999,0.336,0.32300001}|
```

```sql
select correlation
from pg_stats
where tablename = 'film' and attname = 'rental_rate';
```

```
correlation|
-----------|
  0.3195118|
```


#### 4. Сравнение двух похожих запросов.

1) Количество взятий в аренду у такого-то клиента:
```sql
select count(*)
from rental
where customer_id = 164;
```

2) Количество всех взятий в аренду в данный промежуток времени:
```sql
select count(*)
from rental
where rental_date between '2005-05-25 09:00' and '2005-05-25 11:30';
```


Планы запроса 1 без индекса:
```sql
Aggregate  (cost=350.61..350.62 rows=1 width=8) (actual time=1.135..1.135 rows=1 loops=1)
  ->  Seq Scan on rental  (cost=0.00..350.55 rows=25 width=0) (actual time=0.124..1.132 rows=16 loops=1)
        Filter: (customer_id = 164)
        Rows Removed by Filter: 16028
Planning Time: 0.067 ms
Execution Time: 1.168 ms
```

Планы запроса 2 без индекса:
```sql
Aggregate  (cost=390.70..390.71 rows=1 width=8) (actual time=1.080..1.080 rows=1 loops=1)
  ->  Seq Scan on rental  (cost=0.00..390.66 rows=16 width=0) (actual time=0.014..1.077 rows=15 loops=1)
        Filter: ((rental_date >= '2005-05-25 09:00:00'::timestamp without time zone) AND (rental_date <= '2005-05-25 11:30:00'::timestamp without time zone))
        Rows Removed by Filter: 16029
Planning Time: 0.057 ms
Execution Time: 1.096 ms
```


Индексы:
```sql
create index IDX_CUSTOMER_ID_ON_RENTAL on rental (customer_id);
create index IDX_RENTAL_DATE_ON_RENTAL on rental (rental_date);
```


План запроса 1 с индексом IDX_CUSTOMER_ID_ON_RENTAL (ускорение в 1,168/0,050 мс = 23,36 раз):
```sql
Aggregate  (cost=72.05..72.06 rows=1 width=8) (actual time=0.029..0.029 rows=1 loops=1)
  ->  Bitmap Heap Scan on rental  (cost=4.48..71.99 rows=25 width=0) (actual time=0.017..0.026 rows=16 loops=1)
        Recheck Cond: (customer_id = 164)
        Heap Blocks: exact=15
        ->  Bitmap Index Scan on idx_customer_id_on_rental  (cost=0.00..4.47 rows=25 width=0) (actual time=0.012..0.012 rows=16 loops=1)
              Index Cond: (customer_id = 164)
Planning Time: 0.062 ms
Execution Time: 0.050 ms
```


План запроса 2 с индексом IDX_RENTAL_DATE_ON_RENTAL (ускорение в 1,096/0,025 мс = 43,84 раз):
```sql
Aggregate  (cost=8.91..8.92 rows=1 width=8) (actual time=0.010..0.010 rows=1 loops=1)
  ->  Index Only Scan using idx_rental_date_on_rental on rental  (cost=0.29..8.87 rows=16 width=0) (actual time=0.005..0.007 rows=15 loops=1)
        Index Cond: ((rental_date >= '2005-05-25 09:00:00'::timestamp without time zone) AND (rental_date <= '2005-05-25 11:30:00'::timestamp without time zone))
        Heap Fetches: 15
Planning Time: 0.092 ms
Execution Time: 0.025 ms
```


#### Объяснение:
Использование индекса IDX_CUSTOMER_ID_ON_RENTAL в запросе 1 повысило эффективность его выполнения: использовался метод доступа Bitmap Scan, так как значения в столбце customer_id не упорядочены (значения распределены случайно) и селективность условия высокая (отобрано 16 записей из 16044).

```sql
select reltuples from pg_class where relname='rental';
```

```
reltuples|
---------|
    16044|
```

```sql
select correlation
from pg_stats
where tablename = 'rental' and attname = 'customer_id';
```

```
correlation|
-----------|
 0.00253747|
```

Использование индекса IDX_RENTAL_DATE_ON_RENTAL в запросе 2 повысило эффективность его выполнения: использовался метод доступа Index Only Scan, так как значения в столбце rental_date упорядочены, селективность условия высокая (отобрано 15 записей из 16044) и, что самое главное, запрашивались только поля из индекса.

```sql
select correlation
from pg_stats
where tablename = 'rental' and attname = 'rental_date';
```

```
correlation|
-----------|
 0.99778163|
```



#### 5. Полная сумма платежей, проведенных таким-то сотрудником в такой-то день:
```sql
select sum(amount)
from payment
where payment_date between '2007-02-16 00:00' and '2007-02-17 00:00'
and staff_id = 1;
```


План запроса без индекса:
```sql
Aggregate  (cost=363.80..363.81 rows=1 width=32) (actual time=1.196..1.196 rows=1 loops=1)
  ->  Seq Scan on payment  (cost=0.00..363.43 rows=146 width=6) (actual time=0.012..1.180 rows=148 loops=1)
        Filter: ((payment_date >= '2007-02-16 00:00:00'::timestamp without time zone) AND (payment_date <= '2007-02-17 00:00:00'::timestamp without time zone) AND (staff_id = 1))
        Rows Removed by Filter: 14448
Planning Time: 0.060 ms
Execution Time: 1.210 ms
```


Индексы:
```sql
create index IDX_PAYMENT_DATE_ON_PAYMENT on payment (payment_date);
create index IDX_STAFF_ID_ON_PAYMENT on payment (staff_id);
```


План запроса с индексом IDX_PAYMENT_DATE_ON_PAYMENT (ускорение в 1,210/0,108 мс = 11,2 раз):
```sql
Aggregate  (cost=120.75..120.76 rows=1 width=32) (actual time=0.085..0.085 rows=1 loops=1)
  ->  Bitmap Heap Scan on payment  (cost=7.25..120.38 rows=146 width=6) (actual time=0.026..0.069 rows=148 loops=1)
        Recheck Cond: ((payment_date >= '2007-02-16 00:00:00'::timestamp without time zone) AND (payment_date <= '2007-02-17 00:00:00'::timestamp without time zone))
        Filter: (staff_id = 1)
        Rows Removed by Filter: 134
        Heap Blocks: exact=15
        ->  Bitmap Index Scan on idx_payment_date_on_payment  (cost=0.00..7.21 rows=293 width=0) (actual time=0.019..0.019 rows=282 loops=1)
              Index Cond: ((payment_date >= '2007-02-16 00:00:00'::timestamp without time zone) AND (payment_date <= '2007-02-17 00:00:00'::timestamp without time zone))
Planning Time: 0.060 ms
Execution Time: 0.108 ms
```


План запроса с дополнительным индексом IDX_STAFF_ID_ON_PAYMENT (без изменений):
```sql
Aggregate  (cost=120.75..120.76 rows=1 width=32) (actual time=0.078..0.078 rows=1 loops=1)
  ->  Bitmap Heap Scan on payment  (cost=7.25..120.38 rows=146 width=6) (actual time=0.024..0.063 rows=148 loops=1)
        Recheck Cond: ((payment_date >= '2007-02-16 00:00:00'::timestamp without time zone) AND (payment_date <= '2007-02-17 00:00:00'::timestamp without time zone))
        Filter: (staff_id = 1)
        Rows Removed by Filter: 134
        Heap Blocks: exact=15
        ->  Bitmap Index Scan on idx_payment_date_on_payment  (cost=0.00..7.21 rows=293 width=0) (actual time=0.017..0.017 rows=282 loops=1)
              Index Cond: ((payment_date >= '2007-02-16 00:00:00'::timestamp without time zone) AND (payment_date <= '2007-02-17 00:00:00'::timestamp without time zone))
Planning Time: 0.083 ms
Execution Time: 0.100 ms
```


План запроса с индексом IDX_STAFF_ID_ON_PAYMENT (без изменений):
```sql
Aggregate  (cost=363.80..363.81 rows=1 width=32) (actual time=1.288..1.288 rows=1 loops=1)
  ->  Seq Scan on payment  (cost=0.00..363.43 rows=146 width=6) (actual time=0.013..1.270 rows=148 loops=1)
        Filter: ((payment_date >= '2007-02-16 00:00:00'::timestamp without time zone) AND (payment_date <= '2007-02-17 00:00:00'::timestamp without time zone) AND (staff_id = 1))
        Rows Removed by Filter: 14448
Planning Time: 0.132 ms
Execution Time: 1.306 ms
```


#### Объяснение:
Были рассмотрены три возможных плана выполнения запроса. Наиболее эффективным при таком запросе является использование индекса IDX_PAYMENT_DATE_ON_PAYMENT: значения в столбце payment_date не упорядочены строго, селективность условия высокая (Bitmap Scan было отобрано 148 записей из 14596).

```sql
select reltuples from pg_class where relname='payment';
```

```
reltuples|
---------|
    14596|
```

```sql
select correlation
from pg_stats
where tablename = 'payment' and attname = 'payment_date';
```

```
correlation|
-----------|
 0.84046137|
```

Добавление индекса IDX_STAFF_ID_ON_PAYMENT не изменило план выполнения, так как в столбце staff_id во всей таблице встречаются всего 2 различных значения, частота появления каждого из которых составляет ~50% (средняя селективность выборки):
```sql
select attname, most_common_vals, most_common_freqs
from pg_stats
where tablename = 'payment' and attname = 'staff_id';
```

```
attname |most_common_vals|most_common_freqs      |
--------|----------------|-----------------------|
staff_id|{2,1}           |{0.50041109,0.49958894}|
```

Использование одного индекса IDX_STAFF_ID_ON_PAYMENT не изменило изначальный план выполнения по указанной выше причине.




#### 6. Получить сведения о всех актерах с таким-то именем:
```sql
select actor_id, first_name, last_name
from actor
where first_name = 'Bob';
```


План запроса без индекса:
```sql
Seq Scan on actor  (cost=0.00..4.50 rows=1 width=17) (actual time=0.013..0.023 rows=1 loops=1)
  Filter: ((first_name)::text = 'Bob'::text)
  Rows Removed by Filter: 199
Planning Time: 0.047 ms
Execution Time: 0.031 ms
```


Индекс:
```sql
create index IDX_FIRST_NAME_ON_ACTOR on actor (first_name);
```


План запроса с индексом IDX_FIRST_NAME_ON_ACTOR (без изменений):
```sql
Seq Scan on actor  (cost=0.00..4.50 rows=1 width=17) (actual time=0.013..0.024 rows=1 loops=1)
  Filter: ((first_name)::text = 'Bob'::text)
  Rows Removed by Filter: 199
Planning Time: 0.058 ms
Execution Time: 0.031 ms
```


#### Объяснение:
Добавление индекса IDX_FIRST_NAME_ON_ACTOR не привело к изменению плана запроса, так как размер таблицы достаточно мал и изменение метода доступа неоправдано:
```sql
select reltuples from pg_class where relname='actor';
```

```
reltuples|
---------|
      200|
```



#### 7. Самая большая арендная плата среди фильмов, разрешённых для детей:
```sql
select max(rental_rate)
from film
where rating = 'PG-13';
```


План запроса без индекса:
```sql
Aggregate  (cost=67.06..67.07 rows=1 width=32) (actual time=0.199..0.199 rows=1 loops=1)
  ->  Seq Scan on film  (cost=0.00..66.50 rows=223 width=6) (actual time=0.009..0.177 rows=223 loops=1)
        Filter: (rating = 'PG-13'::mpaa_rating)
        Rows Removed by Filter: 777
Planning Time: 0.080 ms
Execution Time: 0.212 ms
```


Индекс:
```sql
create index IDX_RENTAL_RATE_ON_FILM on film (rental_rate);
```


План запроса с индексом IDX_RENTAL_RATE_ON_FILM (ускорение в 0,212/0,026 мс = 8,15 раз)
```sql
Result  (cost=1.35..1.36 rows=1 width=32) (actual time=0.016..0.016 rows=1 loops=1)
  InitPlan 1 (returns $0)
    ->  Limit  (cost=0.28..1.35 rows=1 width=6) (actual time=0.014..0.014 rows=1 loops=1)
          ->  Index Scan Backward using idx_rental_rate_on_film on film  (cost=0.28..240.04 rows=223 width=6) (actual time=0.013..0.013 rows=1 loops=1)
                Index Cond: (rental_rate IS NOT NULL)
                Filter: (rating = 'PG-13'::mpaa_rating)
                Rows Removed by Filter: 2
Planning Time: 0.093 ms
Execution Time: 0.026 ms
```


#### Объяснение:
Использование индекса IDX_RENTAL_RATE_ON_FILM повысило эффективность выполнения запроса: при создании индекса по умолчанию используется структура данных B-tree. Благодаря особенностям данной структуры, строки выдаются в отсортированном по ключу виде. Таким образом, для получения максимального значения поля rental_rate при rating = 'PG-13' серверу необходимо было осуществить обратное сканирование по ключу rental_rate (Index Scan Backward), попутно проверяя значение поля rating. Как только искомая запись в таблице была найдена, сканирование остановилось.

План данного запроса: изначально сервером было оценено, что 223 записи из таблицы film имеют значение 'PG-13' в столбце rating. Началось обратное сканирование по ключу rental_rate: первая с конца запись - rating = 'NC-17', следующая за ней - rating = 'R', и, наконец, третья запись - rating = 'PG-13'. Таким образом, было отфильтровано всего 2 записи.
