#### Практическое задание №1

#### 1. Индексы
Была исследована производительность сервера при работе с таблицами с двумя типами индексов: BTREE и BRIN.

BTREE:
```sql
create index IDX_NAME_ON_STUDENT on student using btree (name);
create index IDX_BIRTH_DATE_ON_STUDENT on student using btree (birth_date);
```

BRIN:
```sql
create index IDX_NAME_ON_STUDENT on student using brin (name);
create index IDX_BIRTH_DATE_ON_STUDENT on student using brin (birth_date);
```

#### 2. Планы запросов (explain analyze)
Запрос 1:

```sql
select min(birth_date)
from student;
```

Без индекса:
```
Finalize Aggregate  (cost=10282.22..10282.23 rows=1 width=4) (actual time=59.590..59.590 rows=1 loops=1)
  ->  Gather  (cost=10282.00..10282.21 rows=2 width=4) (actual time=59.087..74.882 rows=3 loops=1)
        Workers Planned: 2
        Workers Launched: 2
        ->  Partial Aggregate  (cost=9282.00..9282.01 rows=1 width=4) (actual time=27.921..27.921 rows=1 loops=3)
              ->  Parallel Seq Scan on student  (cost=0.00..8657.00 rows=250000 width=4) (actual time=0.013..15.521 rows=200000 loops=3)
Planning Time: 0.097 ms
Execution Time: 74.908 ms
```

С индексом BTREE:
```
Result  (cost=0.49..0.50 rows=1 width=4) (actual time=0.063..0.063 rows=1 loops=1)
  InitPlan 1 (returns $0)
    ->  Limit  (cost=0.42..0.49 rows=1 width=4) (actual time=0.061..0.061 rows=1 loops=1)
          ->  Index Only Scan using idx_birth_date_on_student on student  (cost=0.42..41226.22 rows=600000 width=4) (actual time=0.060..0.060 rows=1 loops=1)
                Index Cond: (birth_date IS NOT NULL)
                Heap Fetches: 1
Planning Time: 0.101 ms
Execution Time: 0.074 ms
```

С индексом BRIN:
```
Finalize Aggregate  (cost=10282.22..10282.23 rows=1 width=4) (actual time=87.327..87.327 rows=1 loops=1)
  ->  Gather  (cost=10282.00..10282.21 rows=2 width=4) (actual time=77.452..100.493 rows=3 loops=1)
        Workers Planned: 2
        Workers Launched: 2
        ->  Partial Aggregate  (cost=9282.00..9282.01 rows=1 width=4) (actual time=25.632..25.632 rows=1 loops=3)
              ->  Parallel Seq Scan on student  (cost=0.00..8657.00 rows=250000 width=4) (actual time=0.005..13.811 rows=200000 loops=3)
Planning Time: 0.124 ms
Execution Time: 100.522 ms
```
Диаграммы pgAdmin: [1](../attachments/execution_plans/1.execution_plan_1_[without_index].svg), [2](../attachments/execution_plans/2.execution_plan_1_[with_btree_index].svg), [3](../attachments/execution_plans/3.execution_plan_1_[with_brin_index].svg)

Запрос 2:

```sql
select birth_date
from student
where name = 'Peter 103';
```

Без индекса:
```
Gather  (cost=1000.00..10282.10 rows=1 width=4) (actual time=25.611..57.133 rows=1 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Parallel Seq Scan on student  (cost=0.00..9282.00 rows=1 width=4) (actual time=8.403..14.182 rows=0 loops=3)
        Filter: (name = 'Peter 103'::text)
        Rows Removed by Filter: 200000
Planning Time: 0.088 ms
Execution Time: 57.151 ms
```

С индексом BTREE:
```
Index Scan using idx_name_on_student on student  (cost=0.42..8.44 rows=1 width=4) (actual time=0.065..0.065 rows=1 loops=1)
  Index Cond: (name = 'Peter 103'::text)
Planning Time: 0.063 ms
Execution Time: 0.074 ms
```

С индексом BRIN:
```
Bitmap Heap Scan on student  (cost=12.17..7001.59 rows=1 width=4) (actual time=12.306..33.723 rows=1 loops=1)
  Recheck Cond: (name = 'Peter 103'::text)
  Rows Removed by Index Recheck: 400409
  Heap Blocks: lossy=4109
  ->  Bitmap Index Scan on idx_name_on_student  (cost=0.00..12.17 rows=66593 width=0) (actual time=0.090..0.090 rows=42240 loops=1)
        Index Cond: (name = 'Peter 103'::text)
Planning Time: 0.072 ms
Execution Time: 33.752 ms
```

Диаграммы pgAdmin: [1](../attachments/execution_plans/4.execution_plan_2_[without_index].svg), [2](../attachments/execution_plans/5.execution_plan_2_[with_btree_index].svg), [3](../attachments/execution_plans/6.execution_plan_2_[with_brin_index].svg)

#### 3. Анализ времени вставки данных в таблицы
Вставка данных в таблицу без индекса:
- итерация 1: 2,329 сек (200к), 1,746 сек (150к), 3,300 сек (250к)
- итерация 2: 2,371 сек (200к), 1,722 сек (150к), 2,917 сек (250к)
- итерация 3: 2,295 сек (200к), 1,695 сек (150к), 2,890 сек (250к)

Средние значения: 2,332 сек (200к), 1,721 сек (150к), 3,036 сек (250к).

Среднее общее время вставки: 7,089 сек.

Вставка данных в таблицу с индексом BTREE:
- итерация 1: 4,249 сек (200к), 3,234 сек (150к), 5,632 сек (250к)
- итерация 2: 4,357 сек (200к), 3,304 сек (150к), 5,776 сек (250к)
- итерация 3: 4,351 сек (200к), 3,332 сек (150к), 5,748 сек (250к)

Средние значения: 4,319 сек (200к), 3,290 сек (150к), 5,719 сек (250к).

Среднее общее время вставки: 13,328 сек. Замедление вставки: на 6,239 сек (или 88%)

Вставка данных в таблицу с индексом BRIN:
- итерация 1: 2,524 сек (200к), 1,848 сек (150к), 3,179 сек (250к)
- итерация 2: 2,505 сек (200к), 1,808 сек (150к), 3,710 сек (250к)
- итерация 3: 2,479 сек (200к), 1,814 сек (150к), 3,920 сек (250к)

Средние значения: 2,503 сек (200к), 1,823 сек (150к), 3,603 сек (250к).

Среднее общее время вставки: 7,929 сек. Замедление вставки: на 0,84 сек (или 11,85%)

#### 4. Анализ используемого пространства
```
pg_table_size('student') = 50 479 104 байт
```

BTREE:
```
pg_indexes_size('student') = 45 948 928 байт (91% от размера таблицы)
```

BRIN:
```
pg_indexes_size('student') = 13 598 720 байт (27% от размера таблицы)
```

#### Вывод
Использование индекса BTREE значительно повысило скорость выполнения запросов (выборка данных): в ~1012 и ~772 раза, соответственно. Но привело к увеличению занимаего пространства: +91% от размера исходной таблицы.

Использование индекса BRIN привело к замедлению скорости выполнения запроса 1 и лишь немного ускорило выполнение запроса 2.
